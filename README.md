~Current Version: 1.2.2~
~Change log:~
1.2.6 Added better compatibility for html characters. Added cc_company_copyright shortcode. Made shortcodes bail if the content is empty.
1.2.5 Added some useful shortcodes (buttons, clearfix, hr) and made company_address behave more sensibley
1.2.4 Added Action Hooks to allow for plugin extension
1.2.2 - Using plugin for directory of clickcom logos, as these are not theme specific.
1.2.1 Fixed folder structure
1.1 Added "ignore background" setting
1.0 Moved user security to mu plugins
0.7 Finished User Security