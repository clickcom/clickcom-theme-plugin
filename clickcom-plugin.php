<?php

/*

Plugin Name: CC Options

Description: Defines Options for the Site - everything from company information to background colors.

Version: 1.2.7

*/

define('CLICKCOM_PLUGIN_DIR', plugins_url('',__FILE__));

function my_plugin_plugins_api( $false, $action, $args ) {

	$plugin_slug = 'clickcom-plugin';

	// Check if this plugins API is about this plugin
	if( $args->slug != $plugin_slug )
		return false;

	// POST data to send to your API
	$args = array(
		'action' 	=> 'get-plugin-information'
	);

	// Send request for detailed information
	$response = my_plugin_api_request( $args );

	return $response;

}
add_filter( 'plugins_api', 'my_plugin_plugins_api', 10, 3 );

function my_plugin_update_plugins( $transient ) {

	// Check if the transient contains the 'checked' information
	// If no, just return its value without hacking it
	if ( empty( $transient->checked ) )
		return $transient;

	// The transient contains the 'checked' information
	// Now append to it information form your own API
	$plugin_path = plugin_basename( __FILE__ );

	// POST data to send to your API
	$args = array(
		'action' 	=> 'check-latest-version'
	);

	// Send request checking for an update
	$response = my_plugin_api_request( $args );

	// If there is a new version, modify the transient
	if( version_compare( $response->new_version, $transient->checked[$plugin_path], '>' ) )
		 $transient->response[$plugin_path] = $response;

	return $transient;

}

add_filter( 'pre_set_site_transient_update_plugins', 'my_plugin_update_plugins' );

function my_plugin_api_request( $args ) {				
	$request = wp_remote_post('http://design.clickcom.com/wp-content/update.php', array( 'body' => $args ) );

	if ( is_wp_error( $request ) || 200 != wp_remote_retrieve_response_code( $request ) )
		return false;

	$response = unserialize( wp_remote_retrieve_body( $request ) );
	if ( is_object( $response ) )
		return $response;
	else
		return false;

}

/**

 * this function includes scripts needed on the front end of the theme

 * cc_include_scripts()

 * 

 * @return void

 */

function cc_include_scripts() {

    wp_enqueue_script('jquery');

    

    //register cufon & cycle script

	wp_register_script('cufon_script', CLICKCOM_PLUGIN_DIR.'/js/cufon.js', array('jquery'));

	wp_register_script('jquery_cycle', CLICKCOM_PLUGIN_DIR. '/js/jquery.cycle.all.min.js', array('jquery'));

	

    // enqueue the cufon & cycle script

    wp_enqueue_script('cufon_script');

    wp_enqueue_script('jquery_cycle');

}

 

add_action('init', 'cc_include_scripts');

add_action('admin_menu','cc_admin_menu');

add_action('admin_head','cc_admin_head');

add_action( 'admin_enqueue_scripts', 'cc_admin_init');

add_action('wp_head','display_clickcom_css');

add_action('wp_print_styles', 'square_stylesheet');



global $companyinfo, $cc_social_icons;



/**

 * cc_companyinfo()

 * unserializes company info

 * @return void

 */

function cc_companyinfo(){

	global $companyinfo;

	if(!isset($companyinfo)){

		$companyinfo = maybe_unserialize(get_option('clickcom_companyinfo'));

	}

}



/**

 * cc_social_icons()

 * unserializes array of social icons that are being used

 * @return void

 */

function cc_social_icons(){

	global $cc_social_icons;

	if(!isset($cc_social_icons)){

		$cc_social_icons = maybe_unserialize(get_option('clickcom_social_media'));

	}

}



function cc_admin_menu(){

	global $cc_admin_page;

	$cc_admin_page = add_submenu_page('themes.php', 'Template Settings','Template Settings','manage_options','clickcom-template-settings','clickcom_template_settings');

}



/**

 * julep_admin_init()

 * includes jquery scripts in the admin

 * @return void

 */

function cc_admin_init($hook){

	global $cc_admin_page;
	if( $hook != $cc_admin_page ) 
		return;

	if(function_exists( 'wp_enqueue_media' )){
	    wp_enqueue_media();
	}else{
	    wp_enqueue_style('thickbox');
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	}
	wp_register_script( 'jquery','/wp-includes/js/jquery/jquery.js'); //needed for jquery tabs to work

	wp_register_script( 'jquery-ui-core','/wp-includes/js/jquery/ui.core.js'); //needed for jquery tabs to work

	wp_register_script( 'ccUIwidgetscript','/wp-includes/js/jquery/ui.widget.js'); //needed for jquery tabs to work

	wp_register_script( 'jquery-ui-tabs','/wp-includes/js/jquery/ui.tabs.js'); //used for tabbed sections in Template Settings

	//wp_register_script( 'farbtastic','/wp-admin/js/farbtastic.js'); //needed for color wheels to work in Template Settings

	wp_register_script( 'cc-jquery-ui-mouse','/wp-includes/js/jquery/ui.mouse.js');

	wp_register_script( 'jquery-ui-sortable','/wp-includes/js/jquery/ui.sortable.js');

	wp_register_script( 'ccwidget-colorpicker',CLICKCOM_PLUGIN_DIR.'/js/jscolor.js'); //for the new colorpicker (inline)

	wp_register_script( 'cctemplatemediaupload',CLICKCOM_PLUGIN_DIR.'/js/media-upload.js'); //fer the media uploader thingy

	//wp_register_script( 'ccwidget-colorpicker',CLICKCOM_PLUGIN_DIR.'/js/colorpicker.js'); //needed for color wheels to work in Template Settings

	wp_register_script( 'ccwidget-coloreye',CLICKCOM_PLUGIN_DIR.'/js/eye.js'); //needed for color wheels to work in Template Settings

	wp_register_script( 'ccwidget-colorutils',CLICKCOM_PLUGIN_DIR.'/js/utils.js'); //needed for color wheels to work in Template Settings

	wp_register_script( 'ccwidget-colorlayout',CLICKCOM_PLUGIN_DIR.'/js/layout.js'); //needed for color wheels to work in Template Settings


	wp_register_style('colorpicker-css',CLICKCOM_PLUGIN_DIR.'/js/admin-styles/colorpicker.css');

	wp_register_style('jquery-css',CLICKCOM_PLUGIN_DIR.'/js/admin-styles/jquery-ui-1.7.3.custom.css');

	wp_register_style('cc-styles',CLICKCOM_PLUGIN_DIR.'/css/style.css');


	wp_enqueue_style('cc-styles');

	wp_enqueue_style('colorpicker-css');

	wp_enqueue_style('jquery-css');

	wp_enqueue_script('jquery');

	wp_enqueue_script('cctemplatemediaupload');

	wp_enqueue_script('jquery-ui-core');

	wp_enqueue_script('ccUIwidgetscript');

	wp_enqueue_script('jquery-ui-tabs');

	wp_enqueue_script('cc-jquery-ui-mouse');

	wp_enqueue_script('jquery-ui-sortable');



	wp_enqueue_script('ccwidget-colorpicker');

	wp_enqueue_script('ccwidget-coloreye');

	wp_enqueue_script('ccwidget-colorutils');

	wp_enqueue_script('ccwidget-colorlayout');

	//wp_enqueue_script('farbtastic');

}



/**

 * square_stylesheet()

 * includes the stylesheet for the square logo

 * @return void

 */

function square_stylesheet(){

	$gsettings = maybe_unserialize(get_option('clickcom_layout_settings'));

	if($gsettings['logo_shape'] == "square"){

		wp_register_style('squarecss',get_stylesheet_directory_uri().'/style-squarelogo.css');

		wp_enqueue_style('squarecss');

	}

}



/**

 * julep_admin_head()

 * 

 * @return void

 */

function cc_admin_head(){

?>
<link rel='stylesheet' href='/wp-admin/css/farbtastic.css' type='text/css' media='screen' />
<link href='http://fonts.googleapis.com/css?family=Kotta+One|Michroma|Rokkitt:400,700|Copse|Muli:300,400,300italic,400italic|Milonga|Montserrat:400,700' rel='stylesheet' type='text/css'>

<?php

}





if ( ! function_exists( 'clickcom_template_settings' ) ):

function clickcom_template_settings(){

	if($_POST){

		//print_r($_POST);

		//exit;

	?>
	<div class="updated"><p>Template Settings Updated!</p></div>

	<?php

	}

	

	$socialarray = array('facebook','twitter','linkedin','youtube','blogger','digg','flickr','stumbleupon','vimeo','pinterest','google+','rss');

	//LAYOUT AND COLORS POST DATA

	if($_POST['generalSettings']){

		$gspost = $_POST;

		if(is_array($gspost)){

		foreach($gspost as $gkey=>$gval){

			if($gval == "" || $gkey == "generalSettings"){continue;}

			$newgkey = str_replace('_url','',$gkey);

			if(in_array($newgkey,$socialarray)){

			$socialurlarray[$newgkey] = $gval;

			}else{

				$gpostdata[$gkey] = $gval; 

			}

		}

		

		//serialize postdata

		$gpostserialized = maybe_serialize($gpostdata);

		$socialserialized = maybe_serialize($socialurlarray);

		do_action('before_save_clickcom_layout_settings',$gpostserialized);
		do_action('before_save_clickcom_social_media',$socialseralized);

		//update in database

		update_option('clickcom_layout_settings',$gpostserialized);

		update_option('clickcom_social_media',$socialserialized);

		do_action('after_save_clickcom_layout_settings',$gpostserialized);
		do_action('after_save_clickcom_social_media',$socialseralized);

		}

	}

	

	//COMPANY INFO POST DATA

	if($_POST['ccSettings']){

		//print_r($_POST);

		$cpostdata = $_POST;

		$removefromarray = array('blogname','blogdescription','admin_email');

		foreach($cpostdata as $ckey=>$cpost){

			if(empty($cpost) || in_array($ckey,$removefromarray)){continue;}

			$companyarray[$ckey] = $cpost;

		}

		

		//serialize postdata

		$companyserialized = maybe_serialize($companyarray);

		do_action('before_save_clickcom_company_settings',$companyserialized);
		do_action('before_save_clickcom_company_settings_unfiltered',$companyarray);

		//update in database

		update_option('blogname',stripslashes($cpostdata['blogname']));

		update_option('blogdescription',stripslashes($cpostdata['blogdescription']));

		update_option('admin_email',$cpostdata['admin_email']);

		update_option('clickcom_companyinfo',$companyserialized);

		do_action('after_save_clickcom_company_settings',$companyserialized);
		do_action('after_save_clickcom_company_settings_unfiltered',$companyarray);
	}

	

	//tab 1 variables

	$gsettings = maybe_unserialize(get_option('clickcom_layout_settings'));

	$socialicons = maybe_unserialize(get_option('clickcom_social_media'));

	

	$companylogo = $gsettings['company_logo'];

	//$logoshape = $gsettings['logo_shape'];

	//$smlocation = $gsettings['sm_location'];

	$clickcomlogo = $gsettings['clickcom_logo'];

	$movesidebardown = $gsettings['move_sidebar_down'];

	$removesidebaraccent = $gsettings['remove_sidebar_accent_border'];

	$bgignore = $gsettings['bgignore'];

	$bgimage = $gsettings['bgimage'];

	$bgcolor = $gsettings['bgcolor'];

	$bgposition = $gsettings['bgposition'];

	$bgposinput = $gsettings['bgposinput'];

	$bgattachment = $gsettings['bgattachment'];

	$bgrepeat = $gsettings['bgrepeat'];

	$bggrad1 = $gsettings['bggrad1'];

	$bggrad2 = $gsettings['bggrad2'];

	$bgsize = $gsettings['bgsize'];

	$gradienttype = $gsettings['gradienttype'];

	$fontignore = $gsettings['fontignore'];

	$headingfont = $gsettings['headingfont'];

	$bodyfont = $gsettings['bodyfont'];

	$navfont = $gsettings['navfont'];

	//tab 3 variables

	$companyName = get_option('blogname');

	$companySlogan = get_option('blogdescription');

	$companyEmail = get_option('admin_email');

	$companyinfo = maybe_unserialize(get_option('clickcom_companyinfo'));

	

	$companyPhone = $companyinfo['company_phone'];

	$companyFax = $companyinfo['company_fax'];

	$companyTollFree = $companyinfo['company_tollfree'];

	$companyAddress1 = $companyinfo['company_address1'];

	$companyAddress2 = $companyinfo['company_address2'];

	$companyCity = $companyinfo['company_city'];

	$companyState = $companyinfo['company_state'];

	$companyZip = $companyinfo['company_zip'];

	

	$privacyPage = $companyinfo['privacyPage'];

	$termsPage = $companyinfo['termsPage'];

	do_action('cc_tab_variables');

?>

<div class="wrap">

	<h2>Template Settings</h2>

<script>

	jQuery(function() {

		jQuery( "#tabbys" ).tabs();

	});

</script>

<div class="demo">

<div id="tabbys">

	<ul>

		<li><a href="#tabs-1">Layout &amp; Colors</a></li>

		<li><a href="#tabs-3">Company Information</a></li>

	</ul>

	<?php

	//GENERAL SETTINGS TAB

	?>

	<div id="tabs-1">

	<form name="ccgeneralSettings" enctype="multipart/form-data" id="ccgeneralSettings" method="post">

	<input type="hidden" name="generalSettings" value="update" />

		<h2>Logo</h2>

		<table>

		<tr><td style="border:1px solid #ccc; padding:5px;">

		<strong>Current Logo</strong><br /><br />

		<img src="<?php echo get_company_logo(); ?>" />

		</td></tr>

		</table>

		<p>Specify the image path of your logo:<br />

		<input type="text" name="company_logo" class="image-lightbox" id="company_logo" value="<?php echo $companylogo; ?>" size="70" />

		</p>

		<!--p>What is the general Shape of your Logo?

		<br />

		<input type="radio" <?php if($logoshape == "round"){echo "checked";} ?> name="logo_shape" value="round" /> Circular or Elliptical <br />

		<input type="radio" <?php if($logoshape == "square"){echo "checked";} ?> name="logo_shape" value="square" /> Square or Rectangular

		</p-->

		<h2>Social Media Icons</h2>

		<p>This template allows you to add up to 8 social media icons to the top of your website. Provide the link information for the social media icons you want to display on your website.</p>

		<ul>

		<?php

			foreach($socialarray as $social){

		?>

			<li style="float:left;padding-right:25px;"><img width="24" src="<?php echo get_stylesheet_directory_uri(); ?>/images/social/<?php echo $social; ?>.png" />&nbsp;&nbsp;&nbsp;Link: <input type="text" name="<?php echo $social; ?>_url" value="<?php echo $socialicons[$social]; ?>" size="50" /></li>

		<?php

			}

		?>

		</ul>

		<br clear="all" />

		<!--p>Where do you want your social media icons to appear on your website?

		<br /><br />

		<input type="radio" <?php if($smlocation == "header"){echo "checked";} ?> name="sm_location" value="header" /> Header <br />

		<input type="radio" <?php if($smlocation == "footer"){echo "checked";} ?> name="sm_location" value="footer" /> Footer

		</p-->

		<div id="background-section">

			<h2>Background</h2>
			<p><input id="bgignore" type="checkbox"<?php if($bgignore == "true"){echo " checked";} ?> name="bgignore" value="true" ><span style="padding-left:10px;">Ignore background settings</span></p>
			<script>
			jQuery(document).ready(function($){
				if($("input#bgignore").attr("checked")){
					$(".background").prop("disabled", "disabled");
				}
				$("input#bgignore").change(function(){
					if($(this).prop("checked") == true){
						$(".background").prop("disabled", "disabled");
					}
					else{
						$(".background").removeAttr("disabled")
					}
				});
			});
			</script>
			<div id="background-color">

			<strong>Background Color</strong><br />
			<input type="text" style="width:195px;" class="color background" name="bgcolor" id="color1" value="<?php echo $bgcolor; ?>" />
			<input type="text" style="width:195px;" class="color background" name="bggrad1" id="bggrad1" value="<?php echo $bggrad1; ?>" />
			<input type="text" style="width:195px;" class="color background" name="bggrad2" id="bggrad2" value="<?php echo $bggrad2; ?>" />
			<strong>Gradient</strong><br />
			<select id="gradienttype" name="gradienttype" class="background">
				<option value="">None</option>
				<option value="vertical">Vertical</option>
				<option value="radial">Radial</option>
			</select><br />
			<?php $selected = "select option[value='$gradienttype']";  ?>
			<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
			</script>

			</div>
			<div id="background-image">
				<strong>Background Image</strong><br />
				<input class="image-lightbox background" type="text" name="bgimage" value="<?php echo $bgimage ?>">
				<img id="bgimage" src="<?php echo $bgimage ?>" />
			</div>
			<div id="background-layout">
				<div id="background-position">
					<strong>Background Position</strong><br />
					<?php $selected = "select option[value='$bgposition']";  ?>
					<select id="bgposition" name="bgposition" class="background">
						<option value="left top">Left Top</option>
						<option value="left center">Left Top</option>
						<option value="left bottom">Left Top</option>
						<option value="right top">Right Top</option>
						<option value="right center">Right Center</option>
						<option value="right bottom">Right Bottom</option>
						<option value="center top">Center Top</option>
						<option value="center center">Center Center</option>
						<option value="center bottom">Center Bottom</option>
						<option value="input">Input</option>
					</select>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						var bgposinput = '<?php echo $bgposinput; ?>';
						if(!bgposinput){
							$("#bgposinput").hide();
						}
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
					(function($) {
						$("#bgposition").change(function(){
							var txt = '';
							$("#bgposinput").val(txt);
							$("#bgposinput").hide();
							if ($(this).val() == "input") {
								$("#bgposinput").show();
							}
						});
					})( jQuery );
					</script>
					<input type="text" name="bgposinput" id="bgposinput" value="<?php echo $bgposinput; ?>" />
				</div><br />
				<div id="background-attachment">
					<strong>Background Attachment</strong><br />
					<?php $selected = "select option[value='$bgattachment']";  ?>
					<select id="bgattachment" name="bgattachment" class="background">
						<option value="fixed">Fixed</option>
						<option value="scroll">Scroll</option>
					</select>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
					</script>
				</div><br />
				<div id="background-repeat">
					<strong>Background Repeat</strong><br />
					<?php $selected = "select option[value='$bgrepeat']";  ?>
					<select id="bgrepeat" name="bgrepeat" class="background">
						<option value="no-repeat">No Repeat</option>
						<option value="repeat">Repeat</option>
						<option value="repeat-x">Repeat X</option>
						<option value="repeat-y">Repeat Y</option>
					</select>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
					</script>
				</div>
			</div>
			<div id="background-size">
					<strong>Background Size</strong><br />
					<input type="text" name="bgsize" value="<?php echo $bgsize; ?>" />
			</div>
		</div>
		<div id="font-section">
			<p><input id="fontignore" type="checkbox"<?php if($fontignore == "true"){echo " checked";} ?> name="fontignore" value="true" ><span style="padding-left:10px;">Ignore font settings</span></p>
			<script>
			jQuery(document).ready(function($){
				if($("input#fontignore").attr("checked")){
					$(".font").prop("disabled", "disabled");
				}
				$("input#fontignore").change(function(){
					if($(this).prop("checked") == true){
						$(".font").prop("disabled", "disabled");
					}
					else{
						$(".font").removeAttr("disabled")
					}
				});
			});
			</script>
			<h2>Fonts</h2>
			<p>
				<?php $selected = "select option[value='$headingfont']";  ?>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
					</script>
				<strong>Heading Font</strong><br />
				<select id="headingfont" class="font" name="headingfont">
					<option value="droidsans">Droid Sans</option>
				</select>
				<span>Select font for heading, H1-H6.</span>
			</p>
			<p>
				<?php $selected = "select option[value='$bodyfont']";  ?>

				<strong>Body Font</strong><br />
				<select id="bodyfont" class="font" name="bodyfont">
						<option value="Copse">Copse</option>
						<option value="Kotta One">Kotta One</option>
						<option value="Milonga">Milonga</option>
						<option value="Rokkitt">Rokkitt</option>
						<option value="Michroma">Michroma</option>
						<option value="Montserrat">Montserrat</option>
						<option value="Muli">Muli</option>
					</optgroup>
				</select>
				<span id="bodyfonttext">Select font for body text.</span>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
						$("#bodyfonttext").css("font-family", "<?php echo $bodyfont; ?>");
					});
				</script>
				<script>
				(function($) {
					$("#bodyfont").change(function(){
						$("#bodyfonttext").css("font-family", $(this).val());
					});
				})( jQuery );
				</script>
			</p>
			<p>
				<?php $selected = "select option[value='$navfont']";  ?>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("<?php echo $selected; ?>").attr("selected", "selected");
					});
					</script>
				<strong>Navigation Font</strong><br />
				<select id="navfont" class="font" name="navfont">
					<option value="droidsans">Droid Sans</option>
				</select>
				<span>Select font for navigation links.</span>
			</p>
		</div>

		<div id="cc-logo-section">

		<h2>Credits Logo</h2>

		<p>Choose a credits logo.<br /><em>All 3 logos are transparent.</em></p>

		<table>

		<tr>

			<td><input type="radio" value="dark" <?php if($clickcomlogo == "dark"){echo "checked";}?> name="clickcom_logo" />&nbsp;&nbsp;</td>

			<td><img align="center" src="<?php echo CLICKCOM_PLUGIN_DIR; ?>/images/clickcom-dark.png" /></td>

		</tr>

		<tr>

			<td><input type="radio" value="light" <?php if($clickcomlogo == "light"){echo "checked";}?> name="clickcom_logo" />&nbsp;&nbsp;</td>

			<td style="background:#000;"><img align="center" src="<?php echo CLICKCOM_PLUGIN_DIR; ?>/images/clickcom-light.png" /></td>

		</tr>

		<tr>

			<td><input type="radio" value="gray" <?php if($clickcomlogo == "gray"){echo "checked";}?> name="clickcom_logo" />&nbsp;&nbsp;</td>

			<td><img align="center" src="<?php echo CLICKCOM_PLUGIN_DIR; ?>/images/clickcom-gray.png" /></td>

		</tr>

		</table>

	</div>

	<?php do_action('cc_layout_settings'); ?>

		<p id="submit-layout"><input type="submit" class="button" value="Save" /></p>

	</form>

	</div>

	<div id="tabs-3">

	<form name="companySettings" enctype="multipart/form-data" id="companySettings" action="#tabs-3" method="post">

	<input type="hidden" name="ccSettings" value="1" />

		<h2>Company Information</h2>

		<table class="widefat">

		<tr>

			<td width="20%">Company Name</td>

			<td><input type="text" value="<?php echo $companyName?>" name="blogname" style="width:500px;" /></td>

		</tr>

		<tr>

			<td width="20%">Company Slogan</td>

			<td><input type="text" value="<?php echo $companySlogan?>" name="blogdescription" style="width:500px;" /></td>

		</tr>

		<tr>

			<td width="20%">Company Email</td>

			<td><input type="text" value="<?php echo $companyEmail?>" name="admin_email" style="width:500px;" /></td>

		</tr>

		<tr>

			<td width="20%">Phone Numbers</td>

			<td>

			<input type="text" value="<?php echo $companyPhone?>" name="company_phone" style="width:160px;" />&nbsp;

			<input type="text" value="<?php echo $companyFax?>" name="company_fax" style="width:160px;" />&nbsp;

			<input type="text" value="<?php echo $companyTollFree?>" name="company_tollfree" style="width:160px;" /><br />

			<span style="float:left;display:block;width:173px;"><em>Phone</em></span>

			<span style="float:left;display:block;width:170px;"><em>Fax</em></span>

			<span style="float:left;display:block;width:100px;"><em>Toll Free</em></span>

			</td>

		</tr>

		<tr>

			<td width="20%">Address</td>

			<td>

			<input type="text" value="<?php echo $companyAddress1?>" name="company_address1" style="width:500px;" /><br />

			<input type="text" value="<?php echo $companyAddress2?>" name="company_address2" style="width:500px;" /><br />

			<input type="text" value="<?php echo $companyCity?>" name="company_city" style="width:220px;" />,&nbsp;

			<input type="text" value="<?php echo $companyState?>" name="company_state" style="width:100px;" />&nbsp;

			<input type="text" value="<?php echo $companyZip?>" name="company_zip" style="width:155px;" /><br />

			<span style="float:left;display:block;width:220px;"><em>City</em></span>

			<span style="float:left;display:block;width:100px;padding-left:20px;"><em>State</em></span>

			<span style="float:left;display:block;width:100px;padding-left:10px;"><em>Zip</em></span>

			</td>

		</tr>

		<tr>

			<td>Privacy Policy Page</td>

			<td>

			<select name="privacyPage">

				<option value="0"<?php if($privacyPage == 0){echo " selected";} ?>>No Page Selected</option>

			<?php

			$pages = get_pages('sort_column=menu_order');

			//print_r($pages);

			

			foreach($pages as $page){

				?>

				<option value="<?php echo $page->ID; ?>"<?php if($privacyPage == $page->ID){ echo " selected"; } ?>><?php echo $page->post_title; ?></option>

				<?php

			}

			?>

		</select>

			</td>

		</tr>

		<tr>

			<td>Terms of Use Page</td>

			<td>

			<select name="termsPage">

				<option value="0"<?php if($termsPage == 0){echo " selected";} ?>>No Page Selected</option>

			<?php

			//print_r($pages);

			

			foreach($pages as $page){

				?>

				<option value="<?php echo $page->ID; ?>"<?php if($termsPage == $page->ID){ echo " selected"; } ?>><?php echo $page->post_title; ?></option>

				<?php

			}

			?>

		</select>

			</td>

		</tr>

		</table>

		<?php do_action('cc_company_info_settings'); ?>

		<p id="submit-company"><input type="submit" class="button" value="Save" /></p>

	</form>

	</div>

</div>



</div><!-- End demo -->

</div>

<?php

}

endif;



/**

 * this function returns css using the accent colors specified in the template settings

 * display_clickcom_css()

 * 

 * @return void

 */

function display_clickcom_css(){

	$gsettings = maybe_unserialize(get_option('clickcom_layout_settings'));

	$companylogo = $gsettings['company_logo'];

	$clickcomlogo = $gsettings['clickcom_logo'];

	//bg colors/images
	$bgignore = $gsettings['bgignore'];
	$bgimage = $gsettings['bgimage'];
	$bgcolor = '#'.$gsettings['bgcolor'];
	$bgposition = $gsettings['bgposition'];
	$bgposinput = $gsettings['bgposinput'];
	if(!empty($bgposinput)){
		$bgposition = $bgposinput;
	}
	$bgattachment = $gsettings['bgattachment'];
	$bgrepeat = $gsettings['bgrepeat'];
	$bgsize = $gsettings['bgsize'];

	//gradients
	$bggrad1 = '#'.$gsettings['bggrad1'];
	$bggrad2 = '#'.$gsettings['bggrad2'];
	$gradienttype = $gsettings['gradienttype'];

	//fonts
	$fontignore = $gsettings['fontignore'];
	$bodyfont = $gsettings['bodyfont'];
	$headingfont = $gsettings['headingfont'];
	$navfont = $gsettings['navfont'];

	$theme = wp_get_theme()->template;
	if($theme == 'headway')
	$wrapper = '#whitewrap';
	else $wrapper = 'body';

	if($bgignore != 'true'){
	?>

	<style>

	<?php echo $wrapper; ?> {
		font-family:<?php echo $bodyfont; ?>;
		background-color:<?php echo $bgcolor; ?>;
		background-image:url("<?php echo $bgimage; ?>");
		background-position:<?php echo $bgposition; ?>;
		background-attachment: <?php echo $bgattachment; ?>;
		background-repeat:<?php echo $bgrepeat; ?>;
		background-size: <?php echo $bgsize; ?>;

	}

	</style>

	<?php
		if($gradienttype == 'vertical'){
		?>
		<style>
			<?php echo $wrapper; ?> {
				/*background-image: url(<?php echo $gradienturl ?>);*/
				background: -moz-linear-gradient(top, <?php echo $bggrad1; ?> 0%, <?php echo $bggrad2; ?> 100%); /* FF3.6+ */
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $bggrad1; ?>), color-stop(100%,<?php echo $bggrad2; ?>)); /* Chrome,Safari4+ */
				background: -webkit-linear-gradient(top, <?php echo $bggrad1; ?> 0%,<?php echo $bggrad2; ?> 100%); /* Chrome10+,Safari5.1+ */
				background: -o-linear-gradient(top, <?php echo $bggrad1; ?> 0%,<?php echo $bggrad2; ?> 100%); /* Opera 11.10+ */
				background: -ms-linear-gradient(top, <?php echo $bggrad1; ?> 0%,<?php echo $bggrad2; ?> 100%); /* IE10+ */
				background: linear-gradient(to bottom, <?php echo $bggrad1; ?> 0%,<?php echo $bggrad2; ?> 100%); /* W3C */
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=<?php echo $bggrad1; ?>,endColorstr=<?php echo $bggrad2; ?>);
				-ms-filter:"progid:DXImageTransform.Microsoft.gradient(startColorstr=<?php echo $bggrad1; ?>,endColorstr=<?php echo $bggrad2; ?>)";
			}
		</style>
		<?php
		}
	}
}


function wrap_shortcodes($atts, $content){
	//if we ain't got content, bail!
	if(empty($content)){
		return;
	}
	//we only want the following variables from the shortcode for the default attributes
	$before = '';
	$after = '';
	$wrapper = 'span';
	$class = '';
	$id = '';
	$barebones = '';
	//extract the passed array, but only to the above variables, and only if it is an array!
	if(is_array($atts)){
		extract($atts, EXTR_IF_EXISTS);
	}

	//if barebones is true, we are done here
	if ($barebones == 'true'){
		return $content;
	}
	//left and right side of wrappers
	$left = '';
	$right = '';
	/**
	* Take care of the conditionals for the possible empty attributes 
	* like class and id
	*/
	$class = ($class != '' ? ' class="'.$class.'"' : '');
	$id = ($id != '' ? ' id="'.$id.'"' : '');

	/**
	* The following attributes wrap from outside to inside
	* For the left side of the wrapper
	*/
	//open the wrapper
	$left = "<$wrapper $class $id>";
	$left .= html_entity_decode($before,ENT_QUOTES,'UTF-8');
	if($before != '' && $after != ''){
		$left .= '<span>';
	}

	/**
	* The following attributes wrap from inside to outside
	* For the right side of the wrapper
	*/
	if($before != '' && $after != ''){
		$right = '</span>';
	}
	$right .= html_entity_decode($after,ENT_QUOTES,'UTF-8');
	$right .= "</$wrapper>";
	//put it all together and return it
	return $left.$content.$right;

}


//do the shortcode!

add_shortcode( 'cc_company_slogan', 'cc_company_slogan_shortcode' );

function cc_company_slogan_shortcode($atts){

	extract( shortcode_atts( array(

		'id'=>'cc_company_slogan'

	), $atts ) );

	$atts['id'] = $id;

	$companySlogan = get_option('blogdescription');

	return wrap_shortcodes($atts, $companySlogan);

}



/**

 * display_social_media()

 * 

 * @param string $width

 * @param string $height

 * @return social media links and images in html format

 */

function display_social_media($width = '34', $height = '34', $separator = ''){

	global $cc_social_icons;

	cc_social_icons();

	if(!empty($cc_social_icons)){

		foreach($cc_social_icons as $skey=>$socialicon){

			$iconimg = get_stylesheet_directory_uri()."/images/social/".$skey.".png";

			$iconstring .= "<a target=\"_blank\" href=\"$socialicon\"><img width=\"$width\" height=\"$height\" src=\"$iconimg\" /></a>";

			$iconstring .= $separator;

		}

	}

	return $iconstring;

}

//do the shortcode!

add_shortcode( 'cc_social_media', 'display_social_media_shortcode' );

function display_social_media_shortcode($atts){

	extract( shortcode_atts( array(

		'width'=>'34',

		'height' => '34',

		'separator'=>'',

		'id'=>'cc_social_media'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, display_social_media($width, $height, $separator));

}



/**

 * get_clickcom_logo()

 * 

 * @return

 */

function get_clickcom_logo(){

	$gsettings = maybe_unserialize(get_option('clickcom_layout_settings'));

	$clickcomlogo = $gsettings['clickcom_logo'];

	if($clickcomlogo==""){

		$clickcomlogo= 'gray';

	}

	return $clickcomlogo;

}


//do the shortcode!

add_shortcode( 'clickcom_logo', 'get_clickcom_logo_shortcode' );

function get_clickcom_logo_shortcode($atts){

	extract( shortcode_atts( array(
		'id'=>'clickcom_logo'

	), $atts ) );

	$atts['id'] = $id;

	$logo = '<img src="'.CLICKCOM_PLUGIN_DIR.'/images/clickcom-';

	$logo .= get_clickcom_logo();

	$logo .= '.png" alt="Clickcom, Inc." title="Clickcom, Inc." />';

	return wrap_shortcodes($atts, $logo);

}





/**

 * get_company_logo()

 * 

 * @return

 */

function get_company_logo(){

	$gsettings = maybe_unserialize(get_option('clickcom_layout_settings'));

	$companylogo = $gsettings['company_logo'];

	if($companylogo=="" || empty($companylogo)){

		$companylogo = get_stylesheet_directory_uri()."/images/logo-default.png";

	}

	return $companylogo;

}

//do the shortcode!

add_shortcode( 'cc_company_logo', 'get_company_logo_shortcode' );

function get_company_logo_shortcode($atts){
	extract( shortcode_atts( array(
		'alt'=>'',
		'title'=>'',
		'link'=>'true',
		'id'=>'cc_company_logo'

	), $atts ) );

	$atts['id'] = $id;

	$companyLogo = ($link == 'true' ? '<a href="'.home_url().'">' : '');

	$companyLogo .= '<img src="';

	$companyLogo .= get_company_logo();

	$companyLogo .= '"';

	$companyLogo .= ($alt != '' ? ' alt="'.$alt.'"' : '');
	$companyLogo .= ($title != '' ? ' title="'.$title.'"' : '');

	$companyLogo .= ' />';

	$companyLogo .= ($link == 'true' ? '</a>' : '');

	return wrap_shortcodes($atts, $companyLogo);

}



/**

 * get_company_address()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_company_address($spacer=''){

	global $companyinfo;

	cc_companyinfo();

	$fulladdress = $before.$companyinfo['company_address1']."$spacer".$companyinfo['company_address2'].$after;

	if($companyinfo['company_address1'] !== "" && !empty($companyinfo['company_address1'])){

		return $fulladdress;

	}else{

		return '';

	}

}

//do the shortcode!

add_shortcode( 'cc_company_address', 'get_company_address_shortcode' );

function get_company_address_shortcode($atts){
	extract( shortcode_atts( array(

		'id'=>'cc_company_address'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_address($spacer));

}



/**

 * get_company_location()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_company_location($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();



	$fulladdress = $before.$companyinfo['company_city'].", ".$companyinfo['company_state']." ".$companyinfo['company_zip'].$after;



	if($companyinfo['company_city'] !== "" && !empty($companyinfo['company_city'])){

		return $fulladdress;

	}else{

		return false;

	}

}

//do the shortcode!

add_shortcode( 'cc_company_location', 'get_company_location_shortcode' );

function get_company_location_shortcode($atts){
	extract( shortcode_atts( array(

		'id'=>'cc_company_location'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_location());

}



//contcatenate the addresses for the full business address

add_shortcode( 'cc_company_full_address', 'get_company_full_address_shortcode' );

function get_company_full_address_shortcode($atts){

	extract( shortcode_atts( array(

		'separator'=>'<br />',

		'id'=>'cc_company_full_address',
		'spacer'=>''

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_address($spacer).$separator.get_company_location($spacer));

}



/**

 * get_company_phone()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_company_phone($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();



	$phone = $before.$companyinfo['company_phone'].$after;



	if($companyinfo['company_phone'] !== "" && !empty($companyinfo['company_phone'])){

		return $phone;

	}else{

		return false;

	}

}

//do the shortcode!

add_shortcode( 'cc_company_phone', 'get_company_phone_shortcode' );

function get_company_phone_shortcode($atts){
	extract( shortcode_atts( array(

		'id'=>'cc_company_phone'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_phone());

}



/**

 * get_company_tollfree()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_company_tollfree($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();



	$tollfree = $before.$companyinfo['company_tollfree'].$after;



	if($companyinfo['company_tollfree'] !== "" & !empty($companyinfo['company_tollfree'])){

		return $tollfree;

	}else{

		return false;

	}

}

//do the shortcode!

add_shortcode( 'cc_company_tollfree', 'get_company_tollfree_shortcode' );

function get_company_tollfree_shortcode($atts){
	extract( shortcode_atts( array(

		'id'=>'cc_company_tollfree'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_tollfree());

}



/**

 * get_company_fax()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_company_fax($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();

	$fax = $before.$companyinfo['company_fax'].$after;

	if($companyinfo['company_fax'] !== "" & !empty($companyinfo['company_fax'])){

		return $fax;

	}else{

		return '';

	}

}

//do the shortcode!

add_shortcode( 'cc_company_fax', 'get_company_fax_shortcode' );

function get_company_fax_shortcode($atts){

	extract( shortcode_atts( array(

		'before'=>'',

		'after' =>'',

		'id'=>'cc_company_fax'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_company_fax());

}



/**

 * get_privacy_link()

 * 

 * @param string $before

 * @param string $after

 * @return

 */

function get_privacy_link($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();

	//$companyinfo = maybe_unserialize(get_option('clickcom_companyinfo'));

	$privacypage = $companyinfo['privacyPage'];



	if($companyinfo['privacyPage'] > 0){

		$privacylink = get_permalink($privacypage);

		$privacyfulllink = "<a href=\"$privacylink\">Privacy Policy</a>";

		return $before.$privacyfulllink.$after;

	}else{

		return '';

	}

}

//do the shortcode!

add_shortcode( 'cc_company_privacy', 'get_privacy_link_shortcode' );

function get_privacy_link_shortcode($atts){

	extract( shortcode_atts( array(

		'before'=>'',

		'after' =>'',
		
		'id'=>'cc_privacy_link'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_privacy_link());

}



/**

 * this function returns the terms of use link if it set, otherwise it returns nothing

 * get_terms_link()

 * 

 * @param string $before

 * @param string $after

 * @return terms of use link

 */

function get_terms_link($before='',$after=''){

	global $companyinfo;

	cc_companyinfo();



	$termspage = $companyinfo['termsPage'];



	if($companyinfo['termsPage'] > 0){

		$termslink = get_permalink($termspage);

		$termsfulllink = "<a href=\"$termslink\">Terms of Use</a>";

		return $before.$termsfulllink.$after;

	}else{

		return false;

	}

}

//do the shortcode!

add_shortcode( 'cc_company_terms', 'get_terms_link_shortcode' );

function get_terms_link_shortcode($atts){
	extract( shortcode_atts( array(

		'before'=>'',

		'after' =>'',
		
		'id'=>'cc_terms_link'

	), $atts ) );

	$atts['id'] = $id;

	return wrap_shortcodes($atts, get_terms_link());

}

/**

 * this function returns the admin email

 * get_company_email()

 * 

 * @param string $before

 * @param string $after

 * @return admin email

 */


function get_company_email($before='',$after=''){

	return $before.get_option('admin_email').$after;

}

//do the shortcode!

add_shortcode( 'cc_company_email', 'get_company_email_shortcode' );

function get_company_email_shortcode($atts){

	extract( shortcode_atts( array(

		'before'=>'',

		'after' =>'',

		'link'=>'true',

		'linktext'=>'',

		'id'=>'cc_company_email'

	), $atts ) );

	$atts['id'] = $id;

	$company_email = get_company_email();
	$link_text = ($linktext == '' ? $company_email : $linktext);

	if($link == 'true'){
		$company_email = '<a href="mailto:'.$company_email.'">';
		$company_email .= $link_text;
		$company_email .= '</a>';
	}

	return wrap_shortcodes($atts, $company_email);
}

function get_cc_company_copyright(){
	return '&copy;'.date('Y').' '.get_bloginfo('name');
}
add_shortcode( 'cc_company_copyright', 'get_cc_company_copyright_shortcode' );

function get_cc_company_copyright_shortcode($atts){
	$copyright = '&copy;'.date('Y').' '.get_bloginfo('name');
	return wrap_shortcodes($atts,$copyright);
}

if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');

function clearfix_shortcode() {
	return '<div class="clearfix"></div>';
}
add_shortcode( 'clearfix', 'clearfix_shortcode' );

function cc_button_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'class' => 'yellow',
		'text' => 'Learn More Today',
		'link'=>'#',
		'title'=>'Learn More Today',
		'float'=>''
	), $atts ) );

	return '<span class="cc-button-span"><a style="float:'.$float.'" class="cc-button '.$class.'-button" title="'.$title.'" href="'.$link.'">'.$text.'</a></span>';
}
add_shortcode( 'cc_button', 'cc_button_shortcode' );

function horizontalrule_shortcode($atts) {
	extract( shortcode_atts( array(
		'color' => 'black',
		'height'=>'1px'
	), $atts ) );
	return '<hr class="'.$color.'"style="height:'.$height.';color:'.$color.';background-color:'.$color.';" />';
}
add_shortcode( 'horizontalrule', 'horizontalrule_shortcode' );