jQuery(document).ready(function($){
   $('.image-lightbox').click(function(e) {
    var me = $(this);
    console.log(me);
// Create the media frame.
            frame = wp.media.frames.customBackground = wp.media({
                // Set the title of the modal.
                title: $(this).data('choose'),

                // Tell the modal to show only images.
                library: {
                    type: 'image'
                },

                // Customize the submit button.
                button: {
                    // Set the text of the button.
                    text: $(this).data('update'),
                    // Tell the button not to close the modal, since we're
                    // going to refresh the page when the image is selected.
                    close: false
                }
            });

            // When an image is selected, run a callback.
            frame.on( 'select', function() {
                // Grab the selected attachment.
                var attachmenturl = frame.state().get('selection').first().attributes.url;
                me.val(attachmenturl);
                me.attr("src",attachmenturl);
                frame.close();

            });

            // Finally, open the modal.
            frame.open();
    });
});